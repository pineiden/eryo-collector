from time import ctime
import time
import datetime
from datetime import datetime
import pytz
import math
from pytz import timezone

month=[0,31,59,90,120,151,181,212,243,273,304,334]
days_in_month=[31,28,31,30,31,30,31,31,30,31,30,31]

def encode(s):
   return ' '.join([bin(ord(c)).replace('0b', '') for c in s])

def modified_jday(iday,imonth,iyear):
   if(imonth<=2):
      iyr=iyear-1
   ret =365*iyear-678941+iyr//4-iyr//100+iyr//400+iday
   if(imonth!=0):
      ret+=month[imonth-1]
   return ret

def utctime(gweek,sow):
    utc = pytz.utc
    mjd=gweek*7+44244+(int)(sow/86400.0)
    sod=sow-(int)(sow/86400.0)*86400.0
    iyear=(mjd+678940)//365
    idoy=mjd-modified_jday(1,1,iyear)
    while (idoy<=0):
       iyear=iyear-1
       idoy=mjd-modified_jday(1,1,iyear)
    for imonth in range(0,12):
       idoy-=days_in_month[imonth]
       if(idoy>0):
          continue
       iday=idoy+days_in_month[imonth]
       break
    imonth=imonth+1
    ih=(int)(sod/3600.0)
    im=(int)((sod-ih*3600.0)/60.0)
    sec=sod-ih*3600.0-im*60
    msec, sec = math.modf(sec)
    dt = datetime(iyear,imonth,iday,ih,im,int(sec), int(msec*10e6) )
    return utc.localize(dt)
