import struct
from struct import unpack
from .funciones import utctime
from datetime import datetime
from rethinkdb import RethinkDB
rdb = RethinkDB()


def flag_0(astr, length):
    x=struct.unpack('!d',astr[length:length+8])
    y=struct.unpack('!d',astr[length+8:length+16])
    z=struct.unpack('!d',astr[length+16:length+24])
    # Reference site position : X Y Z
    ecef=struct.unpack('!B',astr[length+214:length+25])
    #Reference Coordinate frame
    length+=25
    return (x, y, z, ecef, length)

def flag_1(astr, length, nsat):
    for num in range(0,nsat[0]):
        scon=struct.unpack('!B',astr[length:length+1])
        #Satellite Constellation
        iprn=struct.unpack('!B',astr[length+1:length+2])
        #PRN and Ambiguity
        ptime=struct.unpack('!H',astr[length+2:length+4])
        #Observation period
        fre=struct.unpack('!B',astr[length+4:length+5])
        #Observation frequencies
        azu=struct.unpack('!H',astr[length+5:length+7])
        #Elevation
        ele=struct.unpack('!H',astr[length+7:length+9])
        #Azimuth
        length+=9
    return (scon, iprn, ptime, azu, ele, length)

def flag_2(astr, length, ):
    ndis=struct.unpack('!B',astr[length:1+length])
    #Displacement array length
    length+=1
    N=(struct.unpack('!f',astr[length:4+length]))[0]
    E=(struct.unpack('!f',astr[length+4:8+length]))[0]
    U=(struct.unpack('!f',astr[length+8:12+length]))[0]
    #N E U
    length+=12*ndis[0]
    return (N, E, U, length)

def flag_3(astr, length):
    nvel=struct.unpack('!B',astr[length:1+length])
    #Velocity array length
    length+=1
    Nv=(struct.unpack('!f',astr[length:4+length]))[0]
    Ev=(struct.unpack('!f',astr[length+4:8+length]))[0]
    Uv=(struct.unpack('!f',astr[length+8:12+length]))[0]

    length+=(12*nvel[0])
    return (nvel, Nv,Ev,Uv, length)


def flag_4(astr, length):
    scale=(struct.unpack('!f',astr[length:4+length]))[0]
    #unit weight
    nv=(struct.unpack('!f',astr[length:4+length]))[0]
    ev=(struct.unpack('!f',astr[length:4+length]))[0]
    uv=(struct.unpack('!f',astr[length:4+length]))[0]
    nev=(struct.unpack('!f',astr[length:4+length]))[0]
    nuv=(struct.unpack('!f',astr[length:4+length]))[0]
    euv=(struct.unpack('!f',astr[length:4+length]))[0]
    # N,E,U variance NE NU EU covariance
    length+=7*4
    return (nv, ev, uv, nev, nuv, euv, length)


def flag_5(astr, length):
    ztd=struct.unpack('!f',astr[length:length+4])[0]
    return ztd

import pytz

def create_dict(msg):
    RECV = datetime.utcnow()
    UTC=pytz.utc
    DT_RECV = UTC.localize(RECV).isoformat()
    siteid=msg[12:20]
    siteid_n=siteid[0:4].decode('utf-8')

    msg_dict = dict(
        messageid=unpack('!B',msg[2:3])[0],
        # Message ID
        version=unpack('!B',msg[3:4])[0],
        # Version Number
        gweek=unpack('!H',msg[4:6])[0],
        # GPS week
        gmillionsec=unpack('!I',msg[6:10])[0],
        #GPS millsecond of week (unit: 0.001 second)
        disrate=unpack('!B',msg[10:11])[0],
        #Reciprocal of epoch interval
        leapsec=unpack('!B',msg[11:12])[0],
        siteid=siteid_n, 
        nsat=unpack('!B',msg[20:21])[0],
        # Number of satellites processed
        pdop=unpack('!H',msg[21:23])[0])
    tmp=utctime(msg_dict["gweek"],msg_dict["gmillionsec"]/1000).isoformat()
    flag=ord(msg[23:24])
    MSG={
        "DT_GEN": rdb.iso8601(tmp),
        "DT_RECV": rdb.iso8601(DT_RECV),
        "STATION": msg_dict.get('siteid'),
        "PDOP": msg_dict.get('pdop'),
        "ERYO_VERSION": msg_dict.get('version'),
        "GPS_TIME":{
            "msec":msg_dict.get("gmillionsec"),
            "week":msg_dict.get('gweek'),
            "leap":msg_dict.get('leapsec')
        }
    }
    length = 24
    if flag & 1<<0:
        (x,y,z,ecef,length)=flag_0(msg,length)
        data = dict(zip(["x","y","z","ecef"], (x,y,z,ecef)))
        MSG["POSITION"] = data
    if flag & 1<<1:
        (scon, iprin, ptime, azu, ele, length) = flag_1(msg,
                                                        length,
                                                        msg_dict.get("nsat"))
        data = dict(zip(["scon","iprin","ptime","azu","ele"], (scon,iprin,ptime,azu, ele)))
        MSG["SATELITAL"] = data
    if flag & 1<<2:
        (N,E,U,length) = flag_2(msg, length)
        data = dict(zip(["N","E","U"], (N,E,U)))
        MSG["DELTA_POS"] = data
    if flag & 1<<3:
        (nvel, Nv,Ev,Uv, length) = flag_3(msg, length)
        data = dict(zip(["nvel","Nv","Ev","Uv"], (nvel,Nv,Ev,Uv)))
        MSG["VELOCITY"] = data
    if flag & 1<<4:
        (nv,ev,uv,new,nuv,euv, length) =  flag_4(msg, length)
        MSG["VCV"] = dict(zip(["N","E","U","NE","NU","EU"],(nv,ev,uv,new,nuv,euv)))
    if flag & 1<<5:
        ztd = flag_5(msg, length)
        MSG["ZTD"] = ztd
    return MSG



FLAGS = (flag_0, flag_1, flag_2,
         flag_3, flag_4, flag_5)
