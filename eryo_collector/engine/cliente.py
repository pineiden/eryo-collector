from socket import (socket, AF_INET, SOCK_STREAM, SOCK_NONBLOCK)
import asyncio
from asyncio import wait_for, shield
from basic_logtools.filelog import LogFile
from networktools.path import home_path
from tasktools.taskloop import TaskLoop
from struct import unpack
from pprint import pprint
from .funciones import utctime
from .operaciones import (create_dict)
from data_rdb import Rethink_DBS
from basic_queuetools.queue import read_async_queue
from basic_logtools.filelog import LogFile
from networktools.colorprint import gprint, rprint



async def connection(loop, host, port, timeout=4):
    loop = asyncio.get_event_loop()
    (reader, writer)  = await asyncio.open_connection(
        host=host,
        port=port)
    return (reader, writer)

    #connection(sock, loop)


async def get_eryo_data(create, conn, host, port, timeout=5, **kwargs):
    """
    conn = (reader, writer)
    """
    n = 3000
    stop = b'\x9c\xa5'
    counter = kwargs.get('counter',0)
    loop = kwargs.get('loop')
    queues = kwargs.get('queues')
    notused = kwargs.get('notused')
    nbuffers = kwargs.get('nbuffers')
    if create:
        try:
            conn = await connection(loop, host, port)
            create = False
        except asyncio.TimeoutError as te:
            create = True
            conn = None
            await asyncio.sleep(timeout)
        except asyncio.LimitOverrunError:
            create = True
            conn = None
            await asyncio.sleep(timeout)
    if conn and not create:
        reader, _ = conn
        msg = b''
        try:
            msg = await reader.readuntil(separator=stop)
            while msg==stop:
                msg = await reader.readuntil(separator=stop)
        except asyncio.IncompleteError as ie:
            print(f"Obtenido : {ie.partial}, bytes recibidos {ie.expected}")
            print("String incompleto")
        try:
            data = msg[0:4]
            bytec = unpack('!H',data[0:2])[0]
            # bytec is Byte count, The total number of bytes
            # obtiene los primeros 73
            checksum = unpack('!H',msg[-4:-2])[0]
            suma = sum(list(msg[:-4])) + sum(list(stop))
            if(suma == checksum):
                """
                Detalle del msg
                """
                data = create_dict(msg)
                index = notused.pop()
                queue = queues.get(index)
                if not notused:
                    kwargs['notused'] = set({i for i in range(nbuffers)})
                await queue.put(data)
        except asyncio.TimeoutError as te:
            create = True
            conn = None
            await asyncio.sleep(timeout)
    else:
        await asyncio.sleep(1)
    return (create, conn, host, port), kwargs


async def saving_data(create, rdb_insta, host, port, queue,  **kwargs):
    hostname = kwargs.get('hostname')
    dbname = kwargs.get('dbname')
    suffix = kwargs.get('suffix',"ERYO")
    rate_sec = kwargs.get('rate_sec', 1800)
    rate_count = kwargs.get('rate_count', 1800)
    counter = kwargs.get('counter',{})
    take_time = kwargs.get('take_time',{})
    idq = kwargs.get('id')
    logger = kwargs.get(f'logger_id[{idq}]', None)
    """
    Connect to rdb
    """
    try:
        if create:
            options = {
                "address": (host, port),
                "hostname": hostname
            }
            rdb_insta = Rethink_DBS(**options)
            conn = await rdb_insta.async_connect()
            rdb_insta.set_defaultdb(dbname)
            await rdb_insta.list_dbs()
            await rdb_insta.create_db(dbname)
            await rdb_insta.list_tables(dbname)
            create = False
    except asyncio.TimeoutError as e:
        rdb_insta.close()
        create = True
    except Exception as ex:
        rdb_insta.close()
        create = True

    if rdb_insta and not create:
        """
        On connect read queue from position
        take station id -> check tablenames
        si se crea tabla, crear index, DT_GEN
        take the data and save on rdb

        save:

        await db_insta.save_data()


        """
        # get data from queue
        assert isinstance(queue, asyncio.Queue), "No es una queue correcta"
        if queue.qsize()==0:
            await asyncio.sleep(1)
        async for data in read_async_queue(queue):
            station =  data.get("STATION")
            table_name = f"{station}_{suffix}"
            counter[table_name] = counter.get(table_name, 0) + 1
            try:
                result = await wait_for(
                    rdb_insta.create_table(table_name),
                    timeout=5)
                if result:
                    if result.get('created'):
                        await wait_for(
                            rdb_insta.create_index(
                                table_name, index='DT_GEN'),
                            timeout=5)
                        await wait_for(
                            rdb_insta.list_tables(dbname),
                            timeout=5)
                result_saving = await shield(wait_for(
                    rdb_insta.save_data(table_name, data),
                    timeout=5))
                counter[table_name] += 1
                now=data.get("DT_RECV")
                if counter[table_name]==1:
                    take_time[table_name] = now
                    logger.save(20,f"Inicial conteo de ronda {now}")
                if counter[table_name]>=rate_count:
                    vt = counter[table_name]
                    gprint(f"final round {vt}")
                    delta = now - take_time[table_name]
                    tasa = delta.total_seconds()/rate_sec
                    if logger:
                        loggger.save(20, f'''{idq} Se han guardado {counter[table_name]},
                                          de tabla {table_name}, en un tiempo de {delta},
                                          a una tasag de {tasa} [%]''')
                        logger.save(20, f"{idq} Tamaño de cola {queue.qsize()}")
                    counter[table_name] = 0
            except asyncio.TimeoutError as e:
                rdb_insta.close()
                logger.save(40, f"{idq} error timeout", e)
                create = True
            except asyncio.CancelledError as e:
                logger(40, f"{idq} Error canceled", e)
                rdb_insta.close()
                create = True
            except asyncio.LimitOverrunError as oo:
                logger.save(40, f"{idq} Error de overrun", oo)
                rdb_insta.close()
                create = True
            except Exception as e:
                logger.save(40, f"{idq} Excepcion común", e)
                print("Error x", e)
                create = True

    else:
        await asyncio.sleep(1)
    return (create, rdb_insta, host, port, queue), kwargs


if __name__=="__main__":
    rdb_host = "localhost"
    rdb_port = 28015
    hostname = 'localtest'
    dbname = 'PPP_ERYO'
    suffix = "ERYO"
    host="146.83.8.225"
    port = 9900
    nbuffers = 3
    queues = {i: asyncio.Queue() for i in range(nbuffers)}
    notused = set({i for i in range(nbuffers)})

    loop = asyncio.get_event_loop()
    taskloop = TaskLoop(get_eryo_data,
                        [True, None, host, port],
                        {"timeout":5,"loop":loop,
                         "queues":queues,
                         "notused":notused,
                         "nbuffers":nbuffers})
    task1 = taskloop.create()
    tasks = [task1]
    # create rdb save data tasks

    for idq, queue in queues.items():
        task_rdb = TaskLoop(saving_data,
                            [True,None, rdb_host, rdb_port, queue],
                            {"hostname":hostname,
                             "dbname":dbname,
                             "suffix":suffix,
                             "id":idq,
                             "counter":{},
                             f"logger_id[{idq}]":LogFile(
                                 "eryo",
                                 "ERYO_COLLECTOR",
                                 "localhost",
                                 path="./eryo_log")})
        task_rdb.create()
        tasks.append(task_rdb)


    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()
