import asyncio
from tasktools.taskloop import TaskLoop
from eryo_collector.engine.cliente import get_eryo_data, saving_data
import uvloop
import typer
from pathlib import Path
import daemon
import lockfile
import sys
import time
import os
from lockfile.pidlockfile import PIDLockFile
from lockfile import AlreadyLocked
import signal
from functools import partial
from basic_logtools.filelog import LogFile

def delete(filename, signum, frame):
    filename_lock = f"{filename}.lock"
    if Path(filename).exists():
        os.remove(filename)
    if Path(filename_lock).exists():
        os.remove(filename_lock)
    sys.exit(0)

def start_collector(host: str="146.83.8.225",
                    port: int=9900,
                    rdb_host: str='localhost',
                    rdb_port: int=28015,
                    dbname: str="PPP_ERYO",
                    suffix: str="ERYO",
                    nbuffers: int = 4,
                    hostname: str = 'localtest',
                    background: bool = False,
                    jsondata:Path=Path("./data.json"),
                    usejson=False):
    if usejson and jsondata.exists():
        data = json.loads(jsondata)
    filename='/tmp/eryo.pid'
    handler = partial(delete, filename)
    def run():
        uvl = uvloop.new_event_loop()
        asyncio.set_event_loop(uvl)
        loop = asyncio.get_event_loop()
        queues = {i: asyncio.Queue() for i in range(nbuffers)}
        notused = set({i for i in range(nbuffers)})
        taskloop = TaskLoop(get_eryo_data,
                            [True, None, host, port],
                            {"timeout":5,"loop":loop,
                             "queues":queues,
                             "notused":notused,
                             "nbuffers":nbuffers})
        task1 = taskloop.create()
        tasks = [task1]
        # create rdb save data tasks

        for idq, queue in queues.items():
            task_rdb = TaskLoop(saving_data,
                                [True,None, rdb_host, rdb_port, queue],
                                {"hostname":hostname,
                                 "dbname":dbname,
                                 "suffix":suffix,
                                 "id":idq,
                                 "counter":{},
                                 f"logger_id[{idq}]":LogFile(
                                     "eryo",
                                     "ERYO_COLLECTOR",
                                     "localhost",
                                     path="./eryo_log")})
            task_rdb.create()
            tasks.append(task_rdb)

        try:
            loop.run_forever()
        finally:
            loop.run_until_complete(loop.shutdown_asyncgens())
            loop.close()
    if background:
        print(f"Damonizing, PID: {os.getpid()}")
        context = daemon.DaemonContext(
            stdout=sys.stdout, pidfile=lockfile.FileLock(filename))
        print("runngin daemonized script", context.pidfile)
        with  context as d:
            run()
    else:
        run()

if __name__=="__main__":
    #filename='/tmp/eryo.pid'
    #handler = partial(delete, filename)
    #signal.signal(signal.SIGTERM, handler)
    #signal.signal(signal.SIGINT, handler)
    typer.run(start_collector)
