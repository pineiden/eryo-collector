from setuptools import setup

from pathlib import Path

path = Path(__file__).resolve().parent
with open(path/'README.org', encoding='utf-8') as f:
    long_description = f.read()

with open(path/'VERSION') as version_file:
    version = version_file.read().strip()


setup(name='eryo_collector',
      version=version,
      description='Data Collector, for eryo STREAM',
      url='http://gitlab.csn.uchile.cl/dpineda/eryo_collector',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='MIT',
      packages=['eryo_collector'],
      keywords = ["collector","gnss", "scheduler", "async", "eryo"],
      install_requires=[
          "tasktools",
          "basic_queuetools",
          "datadbs",
          "data_rdb",
          "uvloop",
          "typer"
      ],
      entry_points={
        'console_scripts':["eryo_collector = eryo_collector.scripts.run:collector",]
        },
      include_package_data=True,
      long_description=long_description,
      long_description_content_type='text/markdown',
      zip_safe=False)
