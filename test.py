import sys
import os
import signal
from functools import partial
from pathlib import Path


def delete(filename, signum, frame):
    print("Killing and clean file lock")
    filename_lock = f"{filename}.lock"
    if Path(filename).exists():
        os.remove(filename)
    if Path(filename_lock).exists():
        os.remove(filename_lock)
    sys.exit(0)


def handler(signum, frame):
    print('Signal handler called with signal', signum)
    raise OSError("Couldn't open device!")

if __name__ == "__main__":
    signal.signal(signal.SIGTERM, partial(delete, "test.pid"))
    signal.signal(signal.SIGINT, partial(delete, "test.pid"))

    while name:=input("palabra") != "END":
        print(f"Palabra es {name}")
